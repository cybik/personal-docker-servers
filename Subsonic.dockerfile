# This docker file exposes the VMspec for the XXXXX instance I'll use to run
# the XXXXX server in a container.
#
# This Dockerfile is executed manually, once, to provision the base environment
#  for my environment. It will be invoked with the "reboot always" flag.
#
# Using Build Arguments for the WAR URL from which to provision.
# https://docs.docker.com/engine/reference/commandline/build/#set-build-time-variables-build-arg

FROM ubuntu:14.04.4
MAINTAINER Renaud Lepage <root@cybikbase.com>

# useradd subsonic
RUN groupadd subsonic -g 123
RUN useradd -m subsonic -u 123 -g 123

# auth-keys. using core-generated key.
RUN mkdir -p /home/subsonic/.ssh
ADD files/authorized_keys.core /home/subsonic/.ssh/authorized_keys
RUN chmod 700 /home/subsonic/.ssh/authorized_keys
RUN chown -R subsonic:subsonic /home/subsonic/.ssh

RUN apt-get -y update

# Kinda need this for subsonic, eh?
RUN apt-get -y install openjdk-7-jre-headless

# SSH for future cases! Also, authbind to allow non-root to use port 80
RUN apt-get -y install openssh-server authbind

# Media installs: ffmpeg is used by subsonic to transcode, and we want.
# Also wget because we need it
RUN apt-get -y install python-software-properties software-properties-common wget
RUN add-apt-repository ppa:mc3man/trusty-media
RUN apt-get -y update
RUN apt-get -y install ffmpeg flac lame

WORKDIR /home/subsonic
RUN mkdir -p /home/subsonic/provision
RUN mkdir -p /home/subsonic/archive

ARG CACHEBUST_LAUNCH=1
# Should have moved this down here a while ago
WORKDIR /home/subsonic
ADD files/subsonic.sh /home/subsonic/provision/subsonic.sh
ADD files/sub6-booter.jar /home/subsonic/provision/subsonic-booter-jar-with-dependencies.jar

# More cachebust
ARG CACHEBUST_SCRIPT=1
WORKDIR /home/subsonic
ADD files/subsonic_setup.sh /home/subsonic/subsonic.sh
RUN chmod 0755 /home/subsonic/subsonic.sh

# Volume declarations!
VOLUME /Depot/Server/UserFolders/subsonic
VOLUME /media/Zik
VOLUME /media/Videos
VOLUME /media/4tb/Movies
VOLUME /media/Games/cybik/ddr/Songs

# After all the presetup, get the WAR file. This can change, the above will change less
ARG WAR_FILE
ENV TARGET_WAR_FILE ${WAR_FILE:-https://github.com/EugeneKay/subsonic/releases/download/v6.0-beta1-kang/subsonic-v6.0-beta1-kang.war}
ARG UNPACK_MODE
ENV TARGET_UNPACK_MODE ${UNPACK_MODE:-war}

ARG CACHEBUST_WGET=1
WORKDIR /home/subsonic/archive
# Juuuust to be sure.
RUN rm -f *
RUN wget -c ${TARGET_WAR_FILE}


# Pre-setup that applies to all subsonic forks. Magic, really.
WORKDIR /home/subsonic
RUN /home/subsonic/subsonic.sh --root-data "/Depot/Server/UserFolders/subsonic" \
                               --archive `find /home/subsonic/archive/*.*` \
                               --mode-unpack ${TARGET_UNPACK_MODE}

# Authbind setup
#RUN mkdir -p /etc/authbind/byport
#RUN touch /etc/authbind/byport/80
#RUN chmod 500 /etc/authbind/byport/80
#RUN chown subsonic /etc/authbind/byport/80

WORKDIR /home/subsonic
RUN chown -R subsonic:subsonic /home/subsonic

EXPOSE 9999/tcp 1900/udp
USER subsonic

#CMD [ "authbind", "--deep",

WORKDIR /home/subsonic
CMD ["/home/subsonic/bootscript.sh", "--host=127.0.0.1",  "--port=9999", "--https-port=0", "--context-path=/subsonic" ,"--home=/Depot/Server/UserFolders/subsonic/service",  "--pidfile=/Depot/Server/UserFolders/subsonic/subsonic.pid",  "--default-music-folder=/Depot/Server/UserFolders/subsonic/music",  "--default-podcast-folder=/Depot/Server/UserFolders/subsonic/podcast",  "--default-playlist-folder=/Depot/Server/UserFolders/subsonic/playlists"]
#/home/subsonic/bootscript.sh --host=127.0.0.1 --port=9999 --https-port=0 --context-path=/subsonic --home=/Depot/Server/UserFolders/subsonic/service --pidfile=/Depot/Server/UserFolders/subsonic/subsonic.pid --default-music-folder=/Depot/Server/UserFolders/subsonic/music --default-podcast-folder=/Depot/Server/UserFolders/subsonic/podcast --default-playlist-folder=/Depot/Server/UserFolders/subsonic/playlists
