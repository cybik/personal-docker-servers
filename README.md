ABOUT
=====
This is just a repo to bootstrap a couple of servers under Docker, complete with a script to bootstrap them. Said script ingests directories as intake, and for each webservice I need (like: Gerrit, Gitlab, Subsonic, Jenkins), takes a config/home directory as an argument AFTER THE NAME OF THE DAEMON and does the docker binding bullshit, launching the docker container with all the proper args for fs mounting.

HOW TO PROVISION
================
Since the task itself is kind of dickish to script out and because I won't use this often, we'll keep actual complete scripting out for now.

1. Subsonic
    * Possible Arguments
        * WAR_URL
    * Commands to run
        * Building
            * docker build -t cinuservs/subsonic:vNUM -f Subsonic.dockerfile --build-arg WAR_FILE=<war file URL>
            * Examples
                * This downloads things from DongSonic
                    * docker build -t cinuservs/subsonic:v1 -f Subsonic.dockerfile \
                                  --build-arg WAR_FILE=https://github.com/EugeneKay/subsonic/releases/download/v6.0-beta1-kang/subsonic-v6.0-beta1-kang.war \
                                  --build-arg UNPACK_MODE=war .
                * This downloads it from Subsonic
                    * docker build -t cinuservs/subsonic:vNUM -f Subsonic.dockerfile --build-arg WAR_FILE=http://subsonic.org/download/subsonic-6.0.beta1-war.zip --build-arg UNPACK_MODE=zip .
                * This will use the defaults: dongsonic and warfile
                  * docker build -t cinuservs/subsonic:vNUM -f Subsonic.dockerfile .
        * Running
            * Interactive (probably won't work now!)
                * docker run -it -v /Depot/Server/UserFolders/subsonic:/Depot/Server/UserFolders/subsonic \
                                 -v /media/Games/cybik/ddr/Songs:/media/Games/cybik/ddr/Songs \
                                 -v /media/Zik:/media/Zik -v /media/Videos:/media/Videos \
                                 -v /media/4tb/Movies:/media/4tb/Movies \
                                 -t cinuservs/subsonic:v1
            * As a service
                * Standard Invoke
                    * docker run     -v /Depot/Server/UserFolders/subsonic:/Depot/Server/UserFolders/subsonic \
                                     -v /media/Games/cybik/ddr/Songs:/media/Games/cybik/ddr/Songs \
                                     -v /media/Zik:/media/Zik -v /media/Videos:/media/Videos \
                                     -v /media/4tb/Movies:/media/4tb/Movies \
                                     -t cinuservs/subsonic:v1 --restart=unless-stopped
                 * MODE::OVERKILL
                     * docker run     -v /Depot/Server/UserFolders/subsonic:/Depot/Server/UserFolders/subsonic \
                                      -v /media/Games/cybik/ddr/Songs:/media/Games/cybik/ddr/Songs \
                                      -v /media/Zik:/media/Zik -v /media/Videos:/media/Videos \
                                      -v /media/4tb/Movies:/media/4tb/Movies \
                                      -t cinuservs/subsonic:v1 --restart=unless-stopped

2.

3.
