# This docker file exposes the VMspec for the OpenWRT/LEDE image builder.

FROM ubuntu:14.04.5
MAINTAINER Renaud Lepage <root@cybikbase.com>

# First cachebust - force full rebuild
ARG CACHEBUST_ONE=1

# Stuff that the OpenWRT build system doesn't know about
ENV FORCE_UNSAFE_CONFIGURE=1

# Set up a workspace
RUN mkdir /workspace
WORKDIR /workspace

# Initial payload
RUN apt-get -y update
RUN apt-get -y install wget xz-utils

# Second cachebust - the base builder could get outdated
ARG CACHEBUST_TWO=1

# Get the base stuff from LEDE
RUN wget -O image-builder-x86_64.tar.xz https://downloads.lede-project.org/snapshots/targets/ar71xx/generic/lede-imagebuilder-ar71xx-generic.Linux-x86_64.tar.xz
RUN tar -xJvf image-builder-x86_64.tar.xz

# Full rebuild of the tools
ARG CACHEBUST_THREE=1

# Get the tools for dev
RUN apt-get -y install unzip gawk
RUN apt-get -y install build-essential libssl-dev libncurses5-dev ccache xsltproc

# Source Control
RUN apt-get -y install git-core subversion mercurial

# Fun stuff
RUN apt-get -y install nano byobu

# LEDE build step
ARG CACHEBUST_FOUR=1
WORKDIR /workspace/lede-imagebuilder-ar71xx-generic.Linux-x86_64

VOLUME [ "/workspace/uplink" ]

# LEDE builder
ADD ledebuilder/buildLedeImages.sh .
RUN chmod 755 buildLedeImages.sh

# Shell!
CMD [ "/bin/bash" ]
