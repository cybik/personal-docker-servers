#! /bin/bash

# huh. there's wndrmacv2 direct.
LEDE_PROFILES="wndrmacv2"

# You can change the theme to "luci-theme-bootstrap"
# There's 4 themes at the end of https://downloads.lede-project.org/snapshots/packages/mipsel_24kc/luci/
#LUCI_THEME="luci-theme-material"

# Reference for the packages: https://downloads.lede-project.org/snapshots/packages/mipsel_24kc/
LEDE_PACKAGES="luci luci-theme-material luci-ssl    luci-i18n-base-fr luci-i18n-base-en luci-i18n-base-ja luci-app-firewall firewall luci-i18n-firewall-en luci-i18n-firewall-fr luci-i18n-firewall-ja    luci-app-ddns ddns-scripts luci-i18n-ddns-ja luci-i18n-ddns-fr    luci-app-commands luci-i18n-commands-fr luci-i18n-commands-en luci-i18n-commands-ja    luci-app-openvpn openvpn-openssl luci-i18n-openvpn-ja luci-i18n-openvpn-fr luci-i18n-openvpn-en    luci-app-samba samba36-server luci-i18n-samba-fr luci-i18n-samba-ja luci-i18n-samba-en    luci-app-statistics luci-i18n-statistics-ja luci-i18n-statistics-en luci-i18n-statistics-fr    luci-app-upnp luci-i18n-upnp-ja luci-i18n-upnp-en luci-i18n-upnp-fr miniupnpc    luci-app-shairplay shairplay    luci-app-shairport shairport shairport-sync shairport-sync-openssl    luci-app-qos luci-i18n-qos-en luci-i18n-qos-ja luci-i18n-qos-fr    avahi-dnsconfd minidlna ntfs-3g vim wget libopenssl nano tmux curl"

for LEDE_PROFILE in `echo ${LEDE_PROFILES}`; do \
    echo "Building for profile ${LEDE_PROFILE}"; \
    sleep 5; \
    make image PROFILE="${LEDE_PROFILE}" PACKAGES="${LEDE_PACKAGES}" EXTRA_IMAGE_NAME="cybik-$(date +%s)"; \
done
