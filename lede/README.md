Cool Stuff: just a LEDE image generator
=======================================

* The Dockerfile contains 4 levels of cachebusting
* ./buildLedeContainer builds the Docker container that'll generate the final image
* ./runLedeContainer will, well, run the damn container.

And now the fun stuff
* ledebuilder/buildLedeImages.sh is a simple script that gets copied into the container. Its job is to actually generate the image, but prior to the actual container generation, you can modify it on your host.
* I used the "volume" feature so I can copy the resulting images back into my host easily. That's the "-v" thing in runLedeContainer

Toodles!
