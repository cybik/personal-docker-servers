#! /bin/sh

GERRIT_BOOT=""
GITLAB_BOOT=""
JENKINS_BOOT=""
SUBSONIC_BOOT=""
while [ ! -z $1 ]; do
  #statements
  case $1 in
    gerrit )
      shift;
      GERRIT_BOOT=$1;
      shift;
      ;;
    gitlab )
      shift;
      GITLAB_BOOT=$1;
      shift;
      ;;
    jenkins )
      shift;
      JENKINS_BOOT=$1;
      shift;
      ;;
    subsonic )
      shift;
      SUBSONIC_BOOT=$1;
      shift;
      ;;
    * )
      shift;
      echo "Unsupported server. The next argument is probably unsupported too."
      ;;
  esac
done

if [ ${SUBSONIC_BOOT} -ne "" ]; then
    #statements
    docker run -d --restart="unless-stopped" --name="dongsonic.cinu" -v /Depot/Server/UserFolders/subsonic:/Depot/Server/UserFolders/subsonic cinuservs/subsonic:v
fi
