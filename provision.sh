#! /bin/sh

GERRIT_HOMEPATH=""
GITLAB_HOMEPATH=""
JENKINS_HOMEPATH=""
SUBSONIC_HOMEPATH=""
while [ ! -z $1 ]; do
  #statements
  case $1 in
    gerrit )
      shift;
      GERRIT_HOMEPATH=$1;
      shift;
      ;;
    gitlab )
      shift;
      GITLAB_HOMEPATH=$1;
      shift;
      ;;
    jenkins )
      shift;
      JENKINS_HOMEPATH=$1;
      shift;
      ;;
    subsonic )
      shift;
      SUBSONIC_HOMEPATH=$1;
      shift;
      ;;
    * )
      shift;
      echo "Unsupported server. The next argument is probably unsupported too."
      ;;
  esac
done

# Fun thing: https://docs.docker.com/engine/reference/commandline/run/#restart-policies-restart
# Fun thing 2: https://docs.docker.com/engine/reference/commandline/run/#mount-volume-v-read-only
