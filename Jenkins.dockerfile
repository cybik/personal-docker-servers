# This docker file exposes the VMspec for the XXXXX instance I'll use to run
# the XXXXX server in a container.
#
# This Dockerfile is executed manually, once, to provision the base environment
#  for my environment. It will be invoked with the "reboot always" flag.
#
# Using Build Arguments for the WAR URL from which to provision.
# https://docs.docker.com/engine/reference/commandline/build/#set-build-time-variables-build-arg
# Most likely the default.

FROM ubuntu:14.04.4
MAINTAINER Renaud Lepage <root@cybikbase.com>
ARG WAR_FILE
ENV TARGET_WAR_FILE ${WAR_FILE:-}

# useradd jenkins
RUN useradd -m jenkins

# auth-keys. using jenkins-generated key.
RUN mkdir -p /home/jenkins/.ssh
ADD files/authorized_keys.core /home/jenkins/.ssh/authorized_keys
RUN chmod 700 /home/jenkins/.ssh/authorized_keys
RUN chown -R jenkins:jenkins /home/jenkins/.ssh


EXPOSE 80
