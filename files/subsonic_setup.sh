#! /bin/bash

echo "Unpack whole: $*"

UNPACK_MODE="war"
INVOKE_MODE="manual"
DATADIR="/Depot/Server/UserFolders/subsonic"
while [ ! -z $1 ]; do
    #statements
    case $1 in
        --mode-unpack )
            shift;
            UNPACK_MODE=$1;
            shift;
            ;;
        --archive )
            shift;
            TARGET=$1;
            shift;
            ;;
        --mode-invoke )
            shift;
            INVOKE_MODE=$1;
            shift;
            ;;
        --root-data )
            shift;
            DATADIR=$1;
            shift;
            ;;
        --help )
            usage
            ;;
        * )
            usage
            ;;
    esac
done

usage() {
    echo "Usage: subsonic_unpack.sh [options]"
    echo "  --mode-unpack                         Unpack mode. We could be unpacking a zip."
    echo "  --mode-invoke                         Invoke mode. Is it a war, a full solution?"
    echo "  --root-data-dir                       Root Data Directory."
    echo "  --archive                             The target file we'll be using."
    echo "  --help                                This small usage guide."
    exit 1
}

mkdir /home/subsonic/webapp
if [ "${UNPACK_MODE}" == "war" ]; then
    #statements
    mv /home/subsonic/archive/*.war /home/subsonic/webapp
    mv /home/subsonic/provision/* /home/subsonic/webapp
    ln -s `find /home/subsonic/webapp/*.war` /home/subsonic/webapp/subsonic.war
fi
if [ "${UNPACK_MODE}" == "zip" ]; then
    #statements
    cd /home/subsonic/webapp
    unzip ${TARGET}
fi
if [ "${UNPACK_MODE}" == "tgz" ]; then
    #statements
    cd /home/subsonic/webapp
    tar -xvzf ${TARGET}
fi

# Invoke mode
if [ "${INVOKE_MODE}" == "manual" ]; then
    #statements
    mkdir -p /home/subsonic/webapp
    cd /home/subsonic/webapp
    ls
fi

# Boot it.
cd /home/subsonic/webapp
BOOTSCRIPT="`pwd`/`ls *.sh`"
echo "Bootscript: ${BOOTSCRIPT}"

ln -s ${BOOTSCRIPT} /home/subsonic/bootscript.sh

#echo "reptyr \`cat /Depot/Server/UserFolders/subsonic/subsonic.pid\`" >> ${BOOTSCRIPT}

# SED out the backgrounding.
sed -i 's/\${LOG} 2>\&1 \&/\${LOG} 2>\&1/g' ${BOOTSCRIPT}

# debug for transcode
mkdir -p ${DATADIR}/service/transcode
cd ${DATADIR}/service/transcode
for transcoder in ffmpeg flac lame; do ln -s "$(which $transcoder)"; done
